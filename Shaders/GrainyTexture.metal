#include "Utils.h"

[[ stitchable ]]
half4 grainyTexture(float2 position, half4 color, float2 size, float time, float scale) {
    float2 st = position / size;
    float3 pos = float3(st * scale, time);
    float noiseValue = snoise3(pos);
    return half4(noiseValue, noiseValue, noiseValue, 1.0);
}


[[ stitchable ]]
half4 radialGradientEffect(
                           float2 position,
                           half4 color_,
                           float2 size,
                           float time,
                           float fSmoothness,
                           float rSmoothness,
                           float thickness,
                           float frequency
                           ) {
                               float2 center = size / 2.0;
                               float d = distance(position, center) / length(size);
                               
                               float phase = d * frequency;
                               float sinColor = 0.5 * (sin(phase) + 1.0);
                               
                               sinColor = pow(sinColor, thickness);
                               
                               bool isFalling = cos(phase) < 0.0;
                               
                               if (isFalling) {
                                   sinColor = smoothstep(1.0 - fSmoothness, 1.0, sinColor);
                               } else {
                                   sinColor = smoothstep(0.0, rSmoothness, sinColor);
                               }
                               
                               float brightness = sinColor;
                               return half4(brightness, brightness, brightness, 1.0);
                           }
