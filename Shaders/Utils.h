#pragma once

#include <metal_stdlib>

using namespace metal;

float snoise3(float3 v);

template<typename Tx, typename Ty>
inline Tx mod(Tx x, Ty y)
{
    return x - y * floor(x / y);
}


