#include <metal_stdlib>
#include <SwiftUI/SwiftUI_Metal.h>
using namespace metal;


[[ stitchable ]]
half4 gradientMap(float2 position,
                  half4 color,
                  device const half4 *gradientColors,
                  int colorCount,
                  device const float *controlPoints,
                  int controlPointCount
                  ) {
    
    float intensity = abs(color.r);
    
    if (colorCount < 2) {
        return color;
    }
    
    half3 result = gradientColors[0].rgb;
    float previousStop = controlPoints[0];
    
    for (int i = 1; i < colorCount; ++i) {
        float currentStop = controlPoints[i];
        float blend = smoothstep(previousStop, currentStop, intensity);
        result = mix(result, gradientColors[i].rgb, blend);
        previousStop = currentStop;
    }
    
    return half4(result, 1.0);
}

