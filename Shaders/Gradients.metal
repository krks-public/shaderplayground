
#include "Utils.h"


float oscillate(float f, float smoothness, float thickness) {
    float oscillation = (sin(f) + 1) / 2.0; // Normalize to [0, 1]
    float v = pow(oscillation, thickness);
    return smoothstep(0.5 - smoothness, 0.5 + smoothness, v);
}


float pcurve( float x, float a, float b ){
    float k = pow(a+b,a+b) / (pow(a,a)*pow(b,b));
    return k * pow( x, a ) * pow( 1.0-x, b );
}


[[ stitchable ]]
half4 pCurveRadialGradient(
                      float2 position,
                      half4 color_,
                      float2 size,
                      float2 offset,
                      float time,
                      float frequency,
                      float a,
                      float b
                      ) {
                          float2 adjustedPosition = position - offset * size;
                          float2 center = size / 2.0;
                          float d = distance(adjustedPosition, center) / (length(size) / 2.0);
                          float animated = mod(d + time * 0.2, frequency);
                          float brightness = pcurve(animated, a, b);
                          return half4(half3(brightness), 1.0);
                      }



[[ stitchable ]]
half4 radialGradient(float2 position,
                     half4 color,
                     float2 size,
                     float2 offset,
                     float time,
                     float smoothness,
                     float thickness,
                     float frequency
                     ) {
    
    float2 adjustedPosition = position - offset * size;
    float2 center = size / 2.0;
    float d = distance(adjustedPosition, center) / (length(size) / 2.0);
    float gradient = oscillate(d * frequency + time * (frequency / 10) , smoothness, thickness);
    return half4(gradient, gradient, gradient, 1.0);
}



float curvedSawtoothWave(float x, float period, float transitionWidth) {
    float modX = mod(x, period);
    if (modX < (period - transitionWidth)) {
        return smoothstep(0.0, period - transitionWidth, modX);
    } else {
        return 1.0 - smoothstep(period - transitionWidth, period, modX);
    }
}

[[ stitchable ]]
half4 radialGradientCurvedSawTooth(
                      float2 position,
                      half4 color_,
                      float2 size,
                      float2 offset,
                      float time,
                      float period,
                      float transitionWidth
                      ) {
                          float2 adjustedPosition = position - offset * size;
                          float2 center = size / 2.0;
                          float d = distance(adjustedPosition, center) / (length(size) / 2.0);
                          float t = transitionWidth * period;
                          float animated_d = mod(d + time * 0.2, 1.0);
                          float brightness =  curvedSawtoothWave(animated_d, period, t);
                          return half4(half3(brightness), 1.0);
                      }

[[ stitchable ]]
half4 diamondGradient(
                      float2 position,
                      half4 color,
                      float2 size,
                      float2 offset,
                      float time
                      ) {
    float2 center = size / 2.0;
    float2 adjustedPosition = position - offset * size;
    float2 p = abs(adjustedPosition - center);
    float distance = p.x + p.y;
    float normalizedDistance = distance / (size.x / 2.0);
    float gradient = oscillate(normalizedDistance * 10.0 - time * -2.0, 0.5, 1.0);
    return half4(gradient, gradient, gradient, 1.0);
}


[[ stitchable ]]
half4 mirroredLinearGradient(
                      float2 position,
                      half4 color_,
                      float2 size,
                      float time,
                      float period,
                      float vertical
                      ) {
                          float d = -0.5;
                          
                          if (vertical == 1.0) {
                              d += (position.x / size.x);
                          } else {
                              d += (position.y / size.y);
                          }
                          
                          d *= period;

                          float mirrored_d = mod(d, 2.0);
                          if (mirrored_d > 1.0) {
                              mirrored_d = 2.0 - mirrored_d;
                          }

                          float oscillation = (cos(mirrored_d * 3.141592653589793 + time) + 1) / 2.0;

                          return half4(half3(oscillation), 1.0);
                      }


float doubleExponentialSigmoid (float x, float a){

  float epsilon = 0.00001;
  float min_param_a = 0.0 + epsilon;
  float max_param_a = 1.0 - epsilon;
  a = min(max_param_a, max(min_param_a, a));
  a = 1.0-a; // for sensible results
  
  float y = 0;
  if (x<=0.5){
    y = (pow(2.0*x, 1.0/a))/2.0;
  } else {
    y = 1.0 - (pow(2.0*(1.0-x), 1.0/a))/2.0;
  }
  return y;
}


[[ stitchable ]]
half4 linearGradient2(
                      float2 position,
                      half4 color_,
                      float2 size,
                      float2 colorBounds,
                      float time,
                      float period,
                      float vertical
                      ) {

                          float2 normalizedPosition = (position / size);
                          normalizedPosition -= float2(0.5, 0.5);

                          float p = vertical == 1.0 ? normalizedPosition.x : normalizedPosition.y;
                          
                          
                          float segmentIndex = floor(p * period);
                          
                          // Flip the segment animation direction at each segment boundary
                          if (mod(segmentIndex, 2.0) == 1.0) {
                              p = -p;
                          }
                          

                          
                          float segment = fract(p * period + time);
                          
                          
                          const int numControlPoints = 3;
                          float2 controlPoints[numControlPoints];
                          controlPoints[0] = float2(0.0, 0.0);
                          controlPoints[1] = float2(0.2, 1.0);
                          controlPoints[2] = float2(1.0, 0.0);
                          
                          float blendControlPoints[numControlPoints-1];
                          blendControlPoints[0] = 0.3;
                          blendControlPoints[1] = 0.0;


                          float b = 0.0;
                          for (int i = 0; i < numControlPoints - 1; i++) {
                              if (segment >= controlPoints[i].x && segment < controlPoints[i + 1].x) {
                                  float blend = smoothstep(controlPoints[i].x, controlPoints[i+1].x, segment);
                                  blend = doubleExponentialSigmoid(blend, blendControlPoints[i]);
                                  b = mix(controlPoints[i].y, controlPoints[i+1].y, blend);
                              }
                          }


                          b = clamp(b, colorBounds.x, colorBounds.y);
                          return half4(half3(b), 1.0);
                      }




[[ stitchable ]]
half4 noiseGradient(
                    float2 position,
                    half4 color_,
                    float2 size,
                    float time,
                    float roughness
                    ) {
    float2 normalizedPosition = (position / size) - float2(0.5, 0.5);
    float noisePosition = normalizedPosition.y * roughness;
    float noiseValue = snoise3(float3(noisePosition, noisePosition, time));
    return half4(half3(noiseValue), 1.0);
}



[[ stitchable ]]
half4 lava(
                    float2 position,
                    half4 color_,
                    float2 size,
                    float time,
                    float roughness
                    ) {
                        float2 normalizedPosition = (position / size) - float2(0.5, 0.5);
                        
                        
                        float3 noisePos = float3(normalizedPosition, time);

                        float3 rgb = float3(0.1, cos(time), 0.5);

                        float n = snoise3(noisePos);
                        float3 noise = float3(n);
                        float3 color = noise + rgb;

                        float3 noisePos2 = float3(normalizedPosition, time * 0.5);
                        float noise2 = snoise3(noisePos2);
                        color += smoothstep(.15, .9, noise2);
                        color -= smoothstep(.35, .9, noise2);

                        return half4(half3(color), 1.0);
                    }




[[ stitchable ]]
half4 anotherRadial(
                      float2 position,
                      half4 color_,
                      float2 size,
                      float time,
                      float period,
                      float colorPosition,
                      float2 blendControlPoints_
                      ) {

                          float2 normalizedPosition = (position / size) - float2(0.5, 0.5);

                          float aspectRatio = size.x / size.y;
                          normalizedPosition.x *= aspectRatio;

                          float p = length(normalizedPosition) + time * 0.1;


                          float segment = fract(p * period);
                          float segmentIndex = floor(p * period);

//                          if (mod(segmentIndex, 2.0) == 1.0) {
//                              segment = 1.0 - segment;
//                          }
                          
                          const int numControlPoints = 3;
                          float2 colorPositions[numControlPoints];
                          colorPositions[0] = float2(0.0, 0.0);
                          colorPositions[1] = float2(colorPosition, 1.0);
                          colorPositions[2] = float2(1.0, 0.0);
                          
                          float blendControlPoints[numControlPoints - 1];
                          blendControlPoints[0] = blendControlPoints_.x;
                          blendControlPoints[1] = blendControlPoints_.y;

                          // Animate the segment position with time
                          segment = fract(segment + time * 0.2);

                          float b = 0.0;
                          for (int i = 0; i < numControlPoints - 1; i++) {
                              if (segment >= colorPositions[i].x && segment < colorPositions[i + 1].x) {
                                  float blend = smoothstep(colorPositions[i].x, colorPositions[i + 1].x, segment);
                                  blend = doubleExponentialSigmoid(blend, blendControlPoints[i]);
                                  b = mix(colorPositions[i].y, colorPositions[i + 1].y, blend);
                                  break;
                              }
                          }

                          return half4(half3(b), 1.0);
                      }


