import SwiftUI

enum NoiseGradientShader {
    struct Params {
        var roughness = 50.0
        static let roughnessRange = 0.0 ... 500.0
    }

    @ViewBuilder
    static func sliders(_ params: Binding<Params>) -> some View {
        GSlider(
            label: "Roughness",
            value: params.roughness,
            range: Params.roughnessRange
        )
    }

    static func shader(time: Double, geometry: GeometryProxy, params: Params) -> Shader {
        ShaderLibrary.noiseGradient(
            .float2(geometry.size),
            .float(time),
            .float(params.roughness)
        )
    }

    static func layer() -> Layer {
        return Layer(
            gradient: .noise(Params()),
            name: "Noise"
        )
    }
}

extension NoiseGradientShader {
    struct Preview: View {
        @State var params = NoiseGradientShader.Params()
        let startDate = Date()
        @State var time = 0.0
        var body: some View {
            ZStack(alignment: .bottom) {
                TimelineView(.animation) { _ in
                    Color.white.visualEffect { content, geometry in
                        content.colorEffect(
                            NoiseGradientShader.shader(
                                time: startDate.timeIntervalSinceNow,
//                                time: time,
                                geometry: geometry,
                                params: params
                            )
                        )
                    }
                }
                VStack {
                    NoiseGradientShader.sliders($params)
                    GSlider(label: "Time", value: $time, range: 0 ... 20.0)
                }.padding()
            }.ignoresSafeArea()
        }
    }
}

#Preview {
    NoiseGradientShader.Preview()
}
