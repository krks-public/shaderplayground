import SwiftUI

enum RadialGradient {
    struct Params {
        var smoothness: Double = 0.5
        var thickness: Double = 7.5
        var frequency: Double = 50.0
        static let smoothnessRange = 0.0 ... 1.0
        static let thicknessRange = 0.0 ... 15.0
        static let frequencyRange = 5.0 ... 100.0
    }

    @ViewBuilder
    static func sliders(_ params: Binding<Params>) -> some View {
        GSlider(
            label: "Smoothness",
            value: params.smoothness,
            range: RadialGradient.Params.smoothnessRange
        )
        GSlider(
            label: "Thickness",
            value: params.thickness,
            range: RadialGradient.Params.thicknessRange
        )
        GSlider(
            label: "Frequency",
            value: params.frequency,
            range: RadialGradient.Params.frequencyRange
        )
    }

    static func shader(
        time: Double,
        geometry: GeometryProxy,
        offset: (Double, Double),
        params: Params
    ) -> Shader {
        ShaderLibrary.radialGradient(
            .float2(geometry.size),
            .float2(offset.0, offset.1),
            .float(time),
            .float(params.smoothness),
            .float(params.thickness),
            .float(params.frequency)
        )
    }

    static func layer() -> Layer {
        Layer(
            gradient: .radial(Params()),
            name: "Radial"
        )
    }
}

//
// extension RadialGradient {
//    struct Preview: View {
//        private let startDate = Date()
//
//        private let gradientColors = [
//            Color.rgb255(255, 255, 255, 0.1),
//            Color.rgb255(0, 150, 255, 0.8),
//            Color.rgb255(255, 0, 162, 1.0),
//        ]
//
//        @State var params = RadialGradient.Params(
//            smoothness: 0.5,
//            thickness: 1.0,
//            frequency: 70.0
//        )
//
//        var body: some View {
//            TimelineView(.animation) { _ in
//                ZStack(alignment: .bottom) {
//                    Color.white
//                        .radialGradient(
//                            time: startDate.timeIntervalSinceNow,
//                            params: params
//                        )
//                    VStack {
//                        GSlider(
//                            label: "Smoothness",
//                            value: $params.smoothness,
//                            range: RadialGradient.Params.smoothnessRange
//                        )
//                        GSlider(
//                            label: "Thickness",
//                            value: $params.thickness,
//                            range: RadialGradient.Params.thicknessRange
//                        )
//                        GSlider(
//                            label: "Frequency",
//                            value: $params.frequency,
//                            range: RadialGradient.Params.frequencyRange
//                        )
//                    }.offset(y: -50)
//                        .padding()
//                }
//            }
//            .edgesIgnoringSafeArea(.all)
//        }
//    }
// }
//
// #Preview {
//    RadialGradient.Preview()
// }
