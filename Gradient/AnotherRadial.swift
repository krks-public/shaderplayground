import SwiftUI

enum AnotherRadial {
    struct Params {
        static let periodRange = 0.0 ... 10.0
        var period: Double = 3.0
        var colorPosition = 0.2
        var blendControlPoints = (0.3, 0.0)
        var vertical = true
    }

    @ViewBuilder
    static func sliders(_ params: Binding<Params>) -> some View {
        GSlider(
            label: "Period",
            value: params.period,
            range: Params.periodRange
        )

        GSlider(
            label: "Color position",
            value: params.colorPosition,
            range: 0 ... 1.0
        )

        GSlider(
            label: "Blend control 1",
            value: params.blendControlPoints.0,
            range: 0 ... 1.0
        )

        GSlider(
            label: "Blend control 2",
            value: params.blendControlPoints.1,
            range: 0.0 ... 1.0
        )

        Toggle(isOn: params.vertical, label: {
            Text("Vertical")
        })
    }

    static func shader(time: Double, geometry: GeometryProxy, params: Params) -> Shader {
        ShaderLibrary.anotherRadial(
            .float2(geometry.size),
            .float(time),
            .float(params.period),
            .float(params.colorPosition),
            .float2(params.blendControlPoints.0, params.blendControlPoints.1)
        )
    }

    static func layer() -> Layer {
        return Layer(
            gradient: .anotherRadial(Params()),
            name: "AnotherRadial"
        )
    }
}

extension AnotherRadial {
    struct Preview: View {
        @State var params = AnotherRadial.Params()
        let startDate = Date()
        @State var time = 0.0
        var body: some View {
            ZStack(alignment: .bottom) {
                TimelineView(.animation) { _ in
                    Color.white.visualEffect { content, geometry in
                        content.colorEffect(
                            AnotherRadial.shader(
                                time: startDate.timeIntervalSinceNow * 0.3,
//                                time: time,
                                geometry: geometry,
                                params: params
                            )
                        )
                    }
                }
                VStack {
                    AnotherRadial.sliders($params)
                    GSlider(label: "Time", value: $time, range: 0 ... 20.0)
                }.padding()
            }.ignoresSafeArea()
        }
    }
}

#Preview {
    AnotherRadial.Preview()
}
