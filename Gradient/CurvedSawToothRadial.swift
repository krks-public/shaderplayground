import SwiftUI

enum CurvedSawToothRadial {
    struct Params {
        var period: Double = 0.5
        var transitionWidth: Double = 0.5
        static let periodRange = 0.0 ... 1.0
        static let transitionWidthRange = 0 ... 1.0
    }

    @ViewBuilder
    static func sliders(_ params: Binding<Params>) -> some View {
        GSlider(
            label: "Period",
            value: params.period,
            range: Params.periodRange
        )

        GSlider(
            label: "Transition",
            value: params.transitionWidth,
            range: Params.transitionWidthRange
        )
    }

    static func shader(
        time: Double,
        geometry: GeometryProxy,
        offset: (Double, Double),
        params: Params
    ) -> Shader {
        ShaderLibrary.radialGradientCurvedSawTooth(
            .float2(geometry.size),
            .float2(offset.0, offset.1),
            .float(time),
            .float(params.period),
            .float(params.transitionWidth)
        )
    }

    static func layer() -> Layer {
        return Layer(
            gradient: .curvedSawToothRadial(Params()),
            name: "CurvedSawToothRadial"
        )
    }
}
