
import SwiftUI

enum DiamondGradient {
    struct Params {}

    @ViewBuilder
    static func sliders(_: Binding<Params>) -> some View {}

    static func shader(
        time: Double,
        geometry: GeometryProxy,
        offset: (Double, Double),
        params _: Params
    ) -> Shader {
        ShaderLibrary.diamondGradient(
            .float2(geometry.size),
            .float2(offset.0, offset.1),
            .float(time)
        )
    }

    static func layer() -> Layer {
        Layer(
            gradient: .diamond(Params()),
            name: "Diamond"
        )
    }
}
