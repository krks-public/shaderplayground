import SwiftUI

enum SLinearGradient {
    struct Params {
        static let periodRange = 0.0 ... 10.0
        var period: Double = 3.0
        var brightnessBounds = (0.0, 1.0)
        var vertical = true
    }

    @ViewBuilder
    static func sliders(_ params: Binding<Params>) -> some View {
        GSlider(
            label: "Period",
            value: params.period,
            range: Params.periodRange
        )

        GSlider(
            label: "Min brightness",
            value: params.brightnessBounds.0,
            range: 0 ... 0.5
        )

        GSlider(
            label: "Max brightness",
            value: params.brightnessBounds.1,
            range: 0.5 ... 1.0
        )

        Toggle(isOn: params.vertical, label: {
            Text("Vertical")
        })
    }

    static func shader(time: Double, geometry: GeometryProxy, params: Params) -> Shader {
//        ShaderLibrary.mirroredLinearGradient(
//            .float2(geometry.size),
//            .float(time),
//            .float(params.period),
//            .float(params.vertical ? 1.0 : 0.0)
//        )
        ShaderLibrary.linearGradient2(
            .float2(geometry.size),
            .float2(params.brightnessBounds.0, params.brightnessBounds.1),
            .float(time * 0.2),
            .float(params.period),
            .float(params.vertical ? 1.0 : 0.0)
        )
    }

    static func layer() -> Layer {
        return Layer(
            gradient: .linear(Params()),
            name: "Linear"
        )
    }
}

extension SLinearGradient {
    struct Preview: View {
        @State var params = SLinearGradient.Params()
        let startDate = Date()
        @State var time = 0.0
        var body: some View {
            ZStack(alignment: .bottom) {
                TimelineView(.animation) { _ in
                    Color.white.visualEffect { content, geometry in
                        content.colorEffect(
                            SLinearGradient.shader(
                                time: startDate.timeIntervalSinceNow,
//                                time: time,
                                geometry: geometry,
                                params: params
                            )
                        )
                    }
                }
                VStack {
                    SLinearGradient.sliders($params)
                    GSlider(label: "Time", value: $time, range: 0 ... 20.0)
                }.padding()
            }.ignoresSafeArea()
        }
    }
}

#Preview {
    SLinearGradient.Preview()
}
