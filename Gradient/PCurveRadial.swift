import SwiftUI

enum PCurveRadial {
    struct Params {
        var frequency: Double = 0.25
        var a: Double = 0.5
        var b: Double = 18.0
        static let frequencyRange = 0.0 ... 1.0
        static let aRange = 0 ... 25.0
        static let bRange = 0 ... 25.0
    }

    @ViewBuilder
    static func sliders(_ params: Binding<Params>) -> some View {
        GSlider(
            label: "Frequency",
            value: params.frequency,
            range: Params.frequencyRange
        )

        GSlider(
            label: "A",
            value: params.a,
            range: Params.aRange
        )

        GSlider(
            label: "B",
            value: params.b,
            range: Params.bRange
        )
    }

    static func shader(
        time: Double,
        geometry: GeometryProxy,
        offset: (Double, Double),
        params: Params
    ) -> Shader {
        ShaderLibrary.pCurveRadialGradient(
            .float2(geometry.size),
            .float2(offset.0, offset.1),
            .float(time),
            .float(params.frequency),
            .float(params.a),
            .float(params.b)
        )
    }

    static func layer() -> Layer {
        return Layer(
            gradient: .pCurveRadial(Params()),
            name: "PCurveRadial"
        )
    }
}
