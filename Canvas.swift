import SwiftUI

struct Canvas: View {
    let layers: [Layer]
    let startDate: Date
    let noise: Double
    let applyGradient: Bool
    let offset: (Double, Double)
    let palette: [(Color, Float)]

    var time: Double {
        startDate.timeIntervalSinceNow
    }

    var body: some View {
        TimelineView(.animation) { _ in
            ZStack {
                ZStack {
                    ForEach(layers.filter { !$0.hidden }, id: \.id) { layer in
                        layer.shaderView(time: time, offset: offset)
                            .opacity(layer.opacity)
                    }
                }
                .compositingGroup()
                .colorEffect(
                    Effects.gradientMap(palette: palette),
                    isEnabled: applyGradient
                )

                Color.white
                    .visualEffect { content, proxy in
                        Effects.grainyTexture(content, proxy, time: time, noise: noise)
                    }
                    .opacity(0.5)
                    .blendMode(.overlay)
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
}
