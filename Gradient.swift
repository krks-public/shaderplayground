import SwiftUI

enum GradientShader {
    case radial(RadialGradient.Params)
    case pCurveRadial(PCurveRadial.Params)
    case anotherRadial(AnotherRadial.Params)
    case curvedSawToothRadial(CurvedSawToothRadial.Params)
    case diamond(DiamondGradient.Params)
    case linear(SLinearGradient.Params)
    case noise(NoiseGradientShader.Params)
}
