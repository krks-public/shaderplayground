
import SwiftUI

struct Params {
    var x: Double
}

enum MyState {
    case foo(Params)
}

struct Sandbox: View {
    @State private var selectedValue = 0
    let values = Array(0 ... 100)

    var body: some View {
        VStack {}
    }
}

#Preview {
    Sandbox()
}
