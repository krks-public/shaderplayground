import SwiftUI

struct Layer: Identifiable, Sendable {
    let id = UUID()
    var gradient: GradientShader
    var speed: Double = 0.0
    var blendMode: BlendMode = .difference
    var hidden: Bool = false
    var invert: Bool = false
    var opacity = 1.0
    let name: String

    static let speedRange = -1.0 ... 1.0
    static let opacityRange = 0.0 ... 1.0
    static let blendModes: [BlendMode] = [
        .normal,
        .difference,
        .overlay,
        .exclusion,
        .hardLight,
    ]
}

extension Layer {
    @ViewBuilder func shaderView(time: Double, offset: (Double, Double)) -> some View {
        let layer = Color.white.visualEffect { content, geometryProxy in
            let t = time * speed
            let shader = switch gradient {
                case let .pCurveRadial(params):
                    PCurveRadial.shader(
                        time: t,
                        geometry: geometryProxy,
                        offset: offset,
                        params: params
                    )
                case let .radial(params):
                    RadialGradient.shader(
                        time: t,
                        geometry: geometryProxy,
                        offset: offset,
                        params: params
                    )

                case let .diamond(params):
                    DiamondGradient.shader(
                        time: t,
                        geometry: geometryProxy,
                        offset: offset,
                        params: params
                    )

                case let .curvedSawToothRadial(params):
                    CurvedSawToothRadial.shader(
                        time: t,
                        geometry: geometryProxy,
                        offset: offset,
                        params: params
                    )

                case let .linear(params):
                    SLinearGradient.shader(time: t, geometry: geometryProxy, params: params)

                case let .noise(params):
                    NoiseGradientShader.shader(time: t, geometry: geometryProxy, params: params)

                case let .anotherRadial(params):
                    AnotherRadial.shader(time: t, geometry: geometryProxy, params: params)
            }
            return content.colorEffect(shader)
        }
        .blendMode(blendMode)
        if invert {
            layer.colorInvert()
        } else {
            layer
        }
    }
}
