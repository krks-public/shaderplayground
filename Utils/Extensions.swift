import SwiftUI

extension Color {
    static func rgb255(_ red: Int, _ green: Int, _ blue: Int, _ alpha: Double = 1.0) -> Color {
        return Color(
            red: Double(red) / 255,
            green: Double(green) / 255,
            blue: Double(blue) / 255,
            opacity: alpha
        )
    }
}

extension FloatingPoint {
    func map(from source: ClosedRange<Self>, to target: ClosedRange<Self>) -> Self {
        return ((self - source.lowerBound) / (source.upperBound - source.lowerBound)) * (target.upperBound - target.lowerBound) + target
            .lowerBound
    }

    func clamped(to limits: ClosedRange<Self>) -> Self {
        return min(max(self, limits.lowerBound), limits.upperBound)
    }
}

extension BlendMode {
    func name() -> String {
        switch self {
            case .difference:
                return "Difference"
            case .normal:
                return "Normal"
            case .multiply:
                return "Multiply"
            case .screen:
                return "Screen"
            case .overlay:
                return "Overlay"
            case .darken:
                return "Darken"
            case .lighten:
                return "Lighten"
            case .colorDodge:
                return "Color Dodge"
            case .colorBurn:
                return "Color Burn"
            case .softLight:
                return "Soft Light"
            case .hardLight:
                return "Hard Light"
            case .exclusion:
                return "Exclusion"
            case .hue:
                return "Hue"
            case .saturation:
                return "Saturation"
            case .color:
                return "Color"
            case .luminosity:
                return "Luminosity"
            case .sourceAtop:
                return "Source Atop"
            case .destinationOver:
                return "Destination Over"
            case .destinationOut:
                return "Destination Out"
            case .plusDarker:
                return "Plus Darker"
            case .plusLighter:
                return "Plus Lighter"
            @unknown default:
                return "Unknown"
        }
    }
}
