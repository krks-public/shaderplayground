import SwiftUI


struct DeviceTypeKey: EnvironmentKey {
    static let defaultValue: DeviceType = .unknown
}

extension EnvironmentValues {
    var deviceType: DeviceType {
        get { self[DeviceTypeKey.self] }
        set { self[DeviceTypeKey.self] = newValue }
    }
}

enum DeviceType {
    case iPhone
    case iPad
    case mac
    case unknown
}

struct DeviceTypeModifier: ViewModifier {
    @Environment(\.horizontalSizeClass) var horizontalSizeClass

    func body(content: Content) -> some View {
        #if os(macOS)
        return content.environment(\.deviceType, .mac)
        #elseif os(iOS)
        if UIDevice.current.userInterfaceIdiom == .phone {
            return content.environment(\.deviceType, .iPhone)
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            return content.environment(\.deviceType, .iPad)
        } else {
            return content.environment(\.deviceType, .unknown)
        }
        #else
        return content.environment(\.deviceType, .unknown)
        #endif
    }
}

extension View {
    func detectDeviceType() -> some View {
        self.modifier(DeviceTypeModifier())
    }
}
