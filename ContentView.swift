import SwiftUI
import UniformTypeIdentifiers

struct ContentView: View {
    private let startDate = Date()
    @Environment(\.deviceType) var deviceType

    @State var layers: [Layer] = [
        AnotherRadial.layer(),
        SLinearGradient.layer(),
    ]

    @State var isolatedLayerId: UUID?

    @State var noise: Double = 51102
    @State var showTools = true
    @State var applyGradient = true
    @State var offset = (0.0, 0.0)
    @State var paletteIndex = 0
    @State var control: Control? = .layerConfiguration

    enum Control {
        case offsets
        case layerConfiguration
        case layerAdd
    }

    let layerAddActions = [
        ("Diamond", DiamondGradient.layer),
        ("PCurveRadial", PCurveRadial.layer),
        ("CurvedSawToothRadial", CurvedSawToothRadial.layer),
        ("Radial", RadialGradient.layer),
        ("AnotherRadial", AnotherRadial.layer),
        ("Linear", SLinearGradient.layer),
        ("Noise", NoiseGradientShader.layer),
    ]

    var body: some View {
        ZStack(alignment: .topLeading) {
            let visibleLayers = switch isolatedLayerId {
                case let .some(layerId):
                    layers.filter { $0.id == layerId }
                case .none:
                    layers
            }
            Canvas(
                layers: visibleLayers,
                startDate: startDate,
                noise: noise,
                applyGradient: applyGradient,
                offset: offset,
                palette: Color.GradientPalette.allPallettes[paletteIndex]
            )
            .onTapGesture {
                withAnimation {
                    if control == nil {
                        showTools.toggle()
                    } else {
                        control = nil
                    }
                }
            }
            if showTools {
                tools().padding()
            }
        }
    }
}

extension ContentView {
    @ViewBuilder
    func tool<Content: View>(
        for ctrl: Control,
        icon: String,
        @ViewBuilder content: () -> Content
    ) -> some View {
        let expanded = control == ctrl
        HStack(alignment: .top) {
            CircularButton(icon: icon) {
                withAnimation {
                    control = expanded ? nil : ctrl
                }
            }
            if expanded {
                content()
                    .frame(maxWidth:
                        deviceType == .mac ? 500 : .infinity)
            }
        }
    }

    @ViewBuilder func layerAddList() -> some View {
        VStack(alignment: .leading, spacing: 0.0) {
            ForEach(layerAddActions, id: \.self.0) { label, mkLayer in
                Button {
                    withAnimation {
                        layers.append(mkLayer())
                        control = nil
                    }
                } label: {
                    Text(label)
                }
                .padding()
                .buttonStyle(PlainButtonStyle())
                .font(.system(size: 15.0))
                Divider()
            }
        }
        .background(
            RoundedRectangle(cornerRadius: 10.0).fill(
                .thinMaterial
            )
        )
    }

    @ViewBuilder
    func tools() -> some View {
        VStack(alignment: .leading) {
            tool(for: .layerAdd, icon: "plus") {
                layerAddList()
            }

            tool(for: .layerConfiguration, icon: "square.3.layers.3d") {
                LayerConfigurationView(
                    layers: $layers,
                    isolatedLayerId: $isolatedLayerId
                ) { event in
                    switch event {
                        case let .layerRemoved(layer):
                            withAnimation {
                                layers.removeAll {
                                    $0.id == layer.id
                                }
                            }
                    }
                }
            }

            tool(for: .offsets,
                 icon: "arrow.up.and.down.and.arrow.left.and.right")
            {
                VStack {
                    GSlider(label: "Offset X", value: $offset.0, range: -1.0 ... 1.0)
                    GSlider(label: "Offset Y", value: $offset.1, range: -1.0 ... 1.0)
                }
                .padding()
                .background(
                    RoundedRectangle(cornerRadius: 10.0).fill(
                        .thinMaterial
                    )
                )
            }

            CircularButton(icon: "paintpalette.fill") {
                paletteIndex = (paletteIndex + 1) % Color.GradientPalette.allPallettes.count
            }
        }
    }
}

struct LayerConfigurationView: View {
    @Binding var layers: [Layer]
    @Binding var isolatedLayerId: UUID?
    @State var expandedLayer: Layer? = nil
    @State var draggedLayerId: UUID? = nil

    let onEvent: (Event) -> Void

    enum Event {
        case layerRemoved(Layer)
    }

    var body: some View {
        VStack(alignment: .trailing, spacing: 2.0) {
            let visibleLayers = $layers.enumerated().reversed().filter { _, layer in
                if expandedLayer != nil {
                    layer.id == expandedLayer?.id
                } else {
                    true
                }
            }
            ForEach(
                Array(visibleLayers),
                id: \.element.id
            ) { index, $layer in
                Layer.ConfigurationView(
                    index: index,
                    layer: $layer,
                    isExpanded: layer.id == expandedLayer?.id,
                    isIsolated: layer.id == isolatedLayerId
                ) { event in
                    switch event {
                        case .removed:
                            onEvent(.layerRemoved(layer))
                        case .isolationToggled:
                            isolatedLayerId = isolatedLayerId == layer.id ? nil : layer.id
                        case .expansionToggled:
                            withAnimation {
                                if layer.id == expandedLayer?.id {
                                    expandedLayer = nil
                                } else {
                                    expandedLayer = layer
                                }
                            }
                    }
                }
                .background(
                    .thinMaterial
                )
                .clipShape(
                    RoundedRectangle(cornerRadius: 10.0)
                )
                .onDrag {
                    self.draggedLayerId = layer.id
                    return NSItemProvider(object: layer.name as NSString)
                }
                .onDrop(
                    of: [UTType.text],
                    delegate: DropViewDelegate(currentLayer: layer, layers: $layers, draggedLayerId: $draggedLayerId)
                )
            }
        }
    }
}

struct DropViewDelegate: DropDelegate {
    let currentLayer: Layer
    @Binding var layers: [Layer]
    @Binding var draggedLayerId: UUID?

    func dropEntered(info _: DropInfo) {
        guard let draggedLayerId = draggedLayerId,
              draggedLayerId != currentLayer.id,
              let fromIndex = layers.firstIndex(where: { $0.id == draggedLayerId }),
              let toIndex = layers.firstIndex(where: { $0.id == currentLayer.id }) else { return }

        withAnimation {
            layers.move(fromOffsets: IndexSet(integer: fromIndex), toOffset: toIndex > fromIndex ? toIndex + 1 : toIndex)
        }
    }

    func performDrop(info _: DropInfo) -> Bool {
        draggedLayerId = nil
        return true
    }

    func dropUpdated(info _: DropInfo) -> DropProposal? {
        return DropProposal(operation: .move)
    }
}

#Preview {
    Group {
        #if os(iOS)
            ContentView()
        #else
            ContentView()
                .frame(width: 900)
                .frame(height: 700)
        #endif
    }
    .detectDeviceType()
    .preferredColorScheme(.dark)
}
