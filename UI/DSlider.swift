import SwiftUI

struct GSlider: View {
    let label: String
    @Binding var value: Double
    let range: ClosedRange<Double>
    let trackColor: Color
    let fillColor: Color
    let textColor: Color
    let trackHeight: CGFloat

    init(
        label: String,
        value: Binding<Double>,
        range: ClosedRange<Double>,
        trackHeight: CGFloat = 30.0,
        trackColor: Color = Color.gray,
        fillColor: Color = Color.blue,
        textColor: Color = Color.white
    ) {
        self.label = label
        _value = value
        self.range = range
        self.trackHeight = trackHeight
        self.trackColor = trackColor
        self.fillColor = fillColor
        self.textColor = textColor
    }

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                // Track
                Rectangle()
                    .fill(trackColor)
                    .frame(height: trackHeight)

                // Fill
                Rectangle()
                    .fill(fillColor)
                    .frame(
                        width: value.map(from: range, to: 0 ... geometry.size.width),
                        height: trackHeight
                    )

                HStack {
                    Text("\(label)")
                    Spacer()
                    Text(String(format: "%.2f", value))
                }
                .foregroundColor(textColor)
                .padding(.horizontal, trackHeight / 2.7)
            }
            .cornerRadius(trackHeight / 2)
            .gesture(DragGesture(minimumDistance: 0)
                .onChanged { gesture in
                    value = Double(gesture.location.x / geometry.size.width).map(from: 0 ... 1, to: range)
                        .clamped(to: range)
                })
        }
        .frame(height: trackHeight)
    }
}

extension GSlider {
    struct Preview: View {
        @State private var sliderValue = 0.0
        var body: some View {
            VStack {
                GSlider(
                    label: "Dopeness",
                    value: $sliderValue,
                    range: 0 ... 100,
                    trackColor: .gray.opacity(0.3),
                    fillColor: .blue,
                    textColor: .white
                )
                .padding()
            }
            .padding()
        }
    }
}

#Preview {
    GSlider.Preview()
        .padding()
        .preferredColorScheme(.dark)
}
