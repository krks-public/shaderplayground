import SwiftUI

struct CircularButton: View {
    let icon: String
    let action: () -> Void
    let isActive = false
    var body: some View {
        Button(action: action) {
            let size = CGFloat(20)
            let padding = CGFloat(15)
            Image(systemName: icon)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: size, height: size)
                .padding(padding)
                .background(Circle().fill(.thinMaterial))
                .foregroundStyle(.foreground)
        }
        .focusable(false)
        .buttonStyle(PlainButtonStyle())
    }
}

#Preview {
    VStack {
        CircularButton(icon: "plus", action: {})
    }
    .frame(width: 1000, height: 1000)
    .preferredColorScheme(.dark)
    .background(.gray)
}
