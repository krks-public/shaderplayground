//
//  Debug.swift
//  ShaderPlayground
//
//  Created by Gabor Kerekes on 01/06/2024.
//

import SwiftUI

struct Debug: View {
    private let startDate = Date()
    private let gradientColors = [
        Color.rgb255(255, 0, 192, 0.4),
        Color.rgb255(0, 88, 169, 0.5),
        Color.rgb255(157, 216, 213, 0.6),
        Color.rgb255(255, 255, 255, 0.8),
        Color.rgb255(0, 0, 0, 1.0),
    ]

    var body: some View {
        TimelineView(.animation) { _ in
            ZStack {
                Image("Test")

                Color.white
                    .visualEffect { content, proxy in
                        content.colorEffect(
                            ShaderLibrary.noiseEffect(
                                .float2(proxy.size),
                                .float(startDate.timeIntervalSinceNow)
                            )
                        )
                    }
//                    .blendMode(.overlay)
            }
        }
    }
}

#Preview {
    Debug()
}
