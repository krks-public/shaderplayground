//
//  ShaderPlaygroundApp.swift
//  ShaderPlayground
//
//  Created by Gabor Kerekes on 01/06/2024.
//

import SwiftUI

@main
struct ShaderPlaygroundApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().detectDeviceType()
        }
    }
}
