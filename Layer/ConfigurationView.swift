import SwiftUI

extension Layer {
    struct ConfigurationView: View {
        let index: Int
        @Binding var layer: Layer
        var isExpanded: Bool
        var isIsolated: Bool
        var onEvent: (Event) -> Void
        @Environment(\.deviceType) var deviceType

        enum Event {
            case removed
            case isolationToggled
            case expansionToggled
        }

        var body: some View {
            VStack(alignment: .leading, spacing: 0.0) {
                HStack {
                    viewHeader()
                    Spacer()
                    HStack(spacing: 0.0) {
                        if deviceType == .iPhone {
                            expandButton()
                        } else {
                            actionButtons()
                            expandButton()
                        }
                    }
                }.padding(.leading, 15.0)

                if isExpanded {
                    Divider()
                    if deviceType == .iPhone {
                        VStack(alignment: .trailing) {
                            viewDetails().padding()
                            HStack(spacing: 0.0) {
                                actionButtons()
                            }
                        }
                    } else {
                        VStack(alignment: .leading) {
                            viewDetails()
                        }.padding()
                    }
                }
            }
        }
    }
}

extension Layer.ConfigurationView {
    func layerBinding<T>(_ current: T, _ toGradient: @escaping (T) -> GradientShader) -> Binding<T> {
        Binding(get: { current }, set: { newValue in
            layer.gradient = toGradient(newValue)
        })
    }

    @ViewBuilder func expandButton() -> some View {
        actionButton(
            image: Image(systemName: "chevron.right").rotationEffect(
                .degrees(isExpanded ? 90 : 0)
            )
        ) {
            onEvent(.expansionToggled)
        }
    }

    @ViewBuilder func actionButtons() -> some View {
        actionButton(
            image: Image(systemName:
                isIsolated ? "square.dashed.inset.filled" : "square.dashed")
        ) {
            onEvent(.isolationToggled)
        }

        actionButton(
            image: Image(systemName:
                layer.hidden ? "eye.slash" : "eye")

        ) {
            layer.hidden.toggle()
        }

        actionButton(
            image: Image(systemName: "circle.lefthalf.filled")
                .rotationEffect(
                    .degrees(layer.invert ? 180 : 0)
                )
                .animation(.default, value: layer.invert)
        ) {
            layer.invert.toggle()
        }

        actionButton(
            image: Image(systemName: "trash"),
            backgroundStyle:
            Color.red.opacity(0.1)
        ) {
            onEvent(.removed)
        }
    }

    @ViewBuilder
    func actionButton<BackgroundStyle: ShapeStyle, Image: View>(
        image: Image,
        backgroundStyle: BackgroundStyle = .ultraThinMaterial,
        action: @escaping () -> Void
    ) -> some View {
        Button(action: action) {
            image
                .frame(width: 10, height: 10)
                .padding(15.0)
                .background(backgroundStyle)
        }
        .buttonStyle(PlainButtonStyle())
        .overlay(
            Rectangle()
                .fill(.white.opacity(0.1))
                .frame(width: 1.0),
            alignment: .leading
        )
        .focusable(false)
    }

    @ViewBuilder
    func viewHeader() -> some View {
        HStack {
            Image(systemName: "line.3.horizontal")
            Text("\(index + 1)  \(layer.name)")
        }
    }

    @ViewBuilder
    func viewDetails() -> some View {
        VStack(alignment: .leading, spacing: 5.0) {
            HStack {
                Spacer()
                Picker("Blending mode", selection: $layer.blendMode) {
                    ForEach(Layer.blendModes, id: \.hashValue) { blendMode in
                        Text(blendMode.name())
                            .tag(blendMode)
                    }
                }
            }
            GSlider(label: "Speed", value: $layer.speed, range: Layer.speedRange)
            GSlider(label: "Opacity", value: $layer.opacity, range: Layer.opacityRange)
            switch layer.gradient {
                case let .radial(radialParams):
                    RadialGradient.sliders(
                        layerBinding(radialParams, GradientShader.radial)
                    )
                case let .diamond(params):
                    DiamondGradient.sliders(
                        layerBinding(params, GradientShader.diamond)
                    )
                case let .pCurveRadial(params):
                    PCurveRadial.sliders(
                        layerBinding(params, GradientShader.pCurveRadial)
                    )

                case let .curvedSawToothRadial(params):
                    CurvedSawToothRadial.sliders(
                        layerBinding(params, GradientShader.curvedSawToothRadial)
                    )

                case let .linear(params):
                    SLinearGradient.sliders(
                        layerBinding(params, GradientShader.linear)
                    )

                case let .noise(params):
                    NoiseGradientShader.sliders(
                        layerBinding(params, GradientShader.noise)
                    )

                case let .anotherRadial(params):
                    AnotherRadial.sliders(
                        layerBinding(params, GradientShader.anotherRadial)
                    )
            }
        }
    }
}
