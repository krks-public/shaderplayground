//
//  GradientMap.swift
//  ShaderPlayground
//
//  Created by Gabor Kerekes on 03/06/2024.
//

import SwiftUI

enum Effects {
    static func grainyTexture(_ content: EmptyVisualEffect, _ proxy: GeometryProxy, time: Double, noise: Double) -> some VisualEffect {
        content.colorEffect(
            ShaderLibrary.grainyTexture(
                .float2(proxy.size),
                .float(time),
                .float(noise)
            )
        )
    }
}
