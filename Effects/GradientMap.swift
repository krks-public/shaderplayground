
import SwiftUI

extension Effects {
    static func gradientMap(palette: [(Color, Float)]) -> Shader {
        let colors = palette.map { $0.0 }
        let controlPoints = palette.map { $0.1 }
        return ShaderLibrary.gradientMap(
            .colorArray(colors),
            .floatArray(controlPoints)
        )
    }
}

extension Color {
    func asGradientColor(at: Float) -> (Color, Float) {
        return (self, at)
    }

    enum GradientPalette {
        static let defaultPalette = arctic
        static let allPallettes = [
            boc2,
            dolphinsDelight,
            arctic,
        ]
        static let boc =
            [
                Color.rgb255(37, 33, 74).asGradientColor(at: 0.0),
                Color.rgb255(135, 198, 150).asGradientColor(at: 0.24),
                Color.rgb255(255, 252, 59).asGradientColor(at: 0.41),
                Color.rgb255(252, 86, 86).asGradientColor(at: 0.58),
                Color.black.asGradientColor(at: 1.0),
            ]

        static let boc2 =
            [
                Color.rgb255(20, 18, 52).asGradientColor(at: 0.0),
                Color.rgb255(86, 185, 210).asGradientColor(at: 0.25),
                Color.rgb255(219, 231, 204).asGradientColor(at: 0.35),
                Color.rgb255(215, 188, 84).asGradientColor(at: 0.48),
                Color.rgb255(154, 50, 84).asGradientColor(at: 0.78),
                Color.black.asGradientColor(at: 1.0),
            ]
        static let arctic =
            [
                Color.rgb255(0, 88, 169).asGradientColor(at: 0.0),
                Color.rgb255(157, 216, 213).asGradientColor(at: 0.3),
                Color.white.asGradientColor(at: 0.50),
                Color.rgb255(147, 210, 192).asGradientColor(at: 0.60),
                Color.black.asGradientColor(at: 1.0),
            ]
        static let dolphinsDelight =
            [
                Color.rgb255(255, 0, 192).asGradientColor(at: 0.0),
                Color.rgb255(0, 88, 169).asGradientColor(at: 0.15),
                Color.rgb255(157, 216, 213).asGradientColor(at: 0.50),
                Color.white.asGradientColor(at: 0.7),
                Color.black.asGradientColor(at: 1.0),
            ]
    }
}

struct GradientMapPreview: View {
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [.black, .white]),
                       startPoint: .leading,
                       endPoint: .trailing)
            .colorEffect(
                Effects.gradientMap(palette: Color.GradientPalette.boc2)
            ).ignoresSafeArea()
    }
}

#Preview {
    GradientMapPreview().ignoresSafeArea()
}
